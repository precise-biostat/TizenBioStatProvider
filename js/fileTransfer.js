/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 * * Neither the name of Samsung Electronics Co., Ltd. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

var SAAgent, SASocket, connectionListener;

var isRemote = false;
// logs string to HTML
function createHTML(log_string) {
	var logDiv = document.getElementById("resultBoard");
	// log to html and console
	logDiv.innerHTML = logDiv.innerHTML + "<br> " + log_string;
	console.log(log_string);

	// scroll to the bottom of the div
	logDiv.scrollTop = logDiv.scrollHeight;
}

function showConnect() {
	var logDiv = document.getElementById("resultBoard");
	// log to html and console
	logDiv.innerHTML = "";

	var connectionStat = document.getElementById("connectionStatus");
	connectionStat.innerHTML = "Connected";
	connectionStat.style.color = "#27AE60  ";
	createHTML("Display: Connected");

	var cntDiv = document.getElementById("counter");
	cntDiv.innerHTML = cntDiv.innerHTML.substr(0, 14) + 0;
	document.getElementById("file-progress").value = 0;
	document.getElementById("percent").innerHTML = "0%";

}

function showDisconnect() {
	var connectionStat = document.getElementById("connectionStatus");
	connectionStat.innerHTML = "Disconnected";
	connectionStat.style.color = "#EC7063";
	createHTML("Display: Disconnected");
}

connectionListener = {
	/* Remote peer agent (Consumer) requests a service (Provider) connection */
	onrequest : function(peerAgent) {

		createHTML("peerAgent: peerAgent.appName<br />"
				+ "is requsting Service conncetion...");

		/* Check connecting peer by appName */
		if (peerAgent.appName === providerAppName) {
			SAAgent.acceptServiceConnectionRequest(peerAgent);
			createHTML("Service connection request accepted.");

		} else {
			SAAgent.rejectServiceConnectionRequest(peerAgent);
			createHTML("Service connection request rejected.");

		}
	},

	/* Connection between Provider and Consumer is established */
	onconnect : function(socket) {
		var onConnectionLost, dataOnReceive;
		showConnect();
		createHTML("Service connection established");

		/* Obtaining socket */
		SASocket = socket;

		onConnectionLost = function onConnectionLost(reason) {
			createHTML("Service Connection disconnected due to following reason:<br />"
					+ reason);
			showDisconnect();
			// can be replaced with accidental disconnect behavior, i.e. push
			// all from local storage
			if (reason === "DEVICE_DETACHED") {
				// Attempt to wait for connection to resume and push the temp
				// caache files to phone
				restartConnection();
			} else if (reason !== "PEER_DISCONNECTED" || !isRemote) {
				stopSensors();
			}
		};

		/* Inform when connection would get lost */
		SASocket.setSocketStatusListener(onConnectionLost);

		// Receives and sends data
		dataOnReceive = function dataOnReceive(channelId, data) {
			var newData;

			if (!SAAgent.channelIds[0]) {
				createHTML("Something goes wrong...NO CHANNEL ID!");
				return;
			}
			dataParser(channelId, data);
		};

		/* Set listener for incoming data from Consumer */
		SASocket.setDataReceiveListener(dataOnReceive);

	},
	onerror : function(errorCode) {
		createHTML("Service connection error<br />errorCode: " + errorCode);
		showDisconnect();
	}
};

function requestOnSuccess(agents) {
	var i = 0;

	for (i; i < agents.length; i += 1) {
		if (agents[i].role === "PROVIDER") {
			createHTML("Service Provider found!<br />" + "Name: "
					+ agents[i].name);
			SAAgent = agents[i];
			break;
		} else {
			createHTML("Reboot to find Service Provider");
		}
	}

	/* Set listener for upcoming connection from Consumer */
	SAAgent.setServiceConnectionListener(connectionListener);
}

function requestOnError(e) {
	createHTML("requestSAAgent Error" + "Error name : " + e.name + "<br />"
			+ "Error message : " + e.message);
}

function send(data) {
	if (SAAgent.channelIds.length !== 0) {
		SASocket.sendSecureData(SAAgent.channelIds[0], data);
	}
	if (debug) {
		createHTML("Send massage:<br />" + data);
	}

}

webapis.sa.requestSAAgent(requestOnSuccess, requestOnError);